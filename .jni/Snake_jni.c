/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Snake.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_Snake_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startSnake(object);
}

JNIEXPORT jobject JNICALL Java_Snake_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actSnake(object);
}

JNIEXPORT void JNICALL Java_Snake_setAngle_1
  (JNIEnv *env, jobject object, jint angle)
{
	javaEnv = env;
    setAngle(object, toInt(angle));
}

JNIEXPORT void JNICALL Java_Snake_setDistance_1
  (JNIEnv *env, jobject object, jint distance)
{
	javaEnv = env;
    setDistance(object, toInt(distance));
}
